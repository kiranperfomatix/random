import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
        const testUsers = [
            { username: 'test', password: 'test', firstName: 'Admin', lastName: 'User', role: 'Admin' },
            { username: 'buyer', password: 'buyer', firstName: 'Buyer', lastName: 'User', role: 'Buyer' },
            { username: 'seller', password: 'seller', firstName: 'Seller', lastName: 'User', role: 'Seller' },
    ];

        // wrap in timeout to simulate server api call
        setTimeout(() => {

            // fake authenticate api end point
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                // get parameters from post request
                let check = -1;
                const params = JSON.parse(connection.request.getBody());
                for (let i = 0; i < testUsers.length; i++) {
                    if (params.username === testUsers[i].username && params.password === testUsers[i].password) {
                        check = i;
                        break;
                    }
                }
                // check user credentials and return fake jwt token if valid
                if (check !== -1) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token', role: testUsers[check].role} })
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200 })
                    ));
                }
            }

            // fake users api end point
            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return test users if valid, this security is implemented server side
                // in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: testUsers })
                    ));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 401 })
                    ));
                }
            }

        }, 500);

    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions]
};
