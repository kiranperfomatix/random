import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './../authentication.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        if (result === true) {
          const currentUser = JSON.parse(localStorage.getItem('currentUser'));
          if (currentUser.role === 'Admin') {
            this.router.navigate(['/Admin']);
          } else if (currentUser.role === 'Buyer') {
            this.router.navigate(['/Buyer']);
          } else if (currentUser.role === 'Seller') {
            this.router.navigate(['/Seller']);
          }
          this.activeModal.dismiss();
        } else {
          this.error = 'Username or password is incorrect';
          this.loading = false;
        }
      });
  }

}
