import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';

import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: 'Admin', component: AdminComponent, canActivate: [AuthGuard] },
  { path: 'Buyer', component: BuyerComponent, canActivate: [AuthGuard] },
  { path: 'Seller', component: SellerComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
