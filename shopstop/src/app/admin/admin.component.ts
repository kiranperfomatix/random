import { Component, OnInit } from '@angular/core';

import { User } from './../user';
import { UserService } from './../user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  users: User[] = [];
  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getUsers()
    .subscribe(users => {
        this.users = users;
    });
  }

}
