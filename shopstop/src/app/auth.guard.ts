import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentUser')) {
      console.log(route);
      const role = JSON.parse(localStorage.getItem('currentUser')).role;
      if (route.url[0].path === role) {
        return true;
      } else {
        console.log('Invalid access');
        this.router.navigate(['']);
        return false;
      }
    }
    this.router.navigate(['']);
    return false;
  }
}
