'use strict';
/**
 * Whether to check is letter or not
 * 
 * @param {string} c Input string
 * @return {boolean} String or not
 */
function isLetter(c) {
    return c.toLowerCase() != c.toUpperCase();
}

/**
 * The
 * 
 * @param {any} str 
 * @return {any}
 */
function palindrome(str) {
    let arr;
    let rev;
    arr = str.split('');
    arr.forEach(function(element, index) {
        if (!isLetter(element) && (element == '')) {
            arr.splice(index, -1);
        }
    });
    rev = Array.from(arr);
    rev.reverse();
    console.log(arr, rev);
    if (arr == rev) {
        return true;
    }
    return false;
}

console.log(palindrome('not a palindrome'));
