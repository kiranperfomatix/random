var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var restaurantSchema = new Schema({
  name: String,
  campaignMail: [
    {
      name: String
    }
  ]
});

var Restaurant = mongoose.model('Restaurant', restaurantSchema, 'Restaurant');

module.exports = Restaurant;